#!/usr/bin/python

import json

with open("template/parameters.json", "r") as read_file:
    data = json.load(read_file)
    for x in range(1,25):
        for y in range(1,3):
            disk_list = [{'lun': 0, 'createOption': 'attach', 'caching': 'ReadWrite', 'writeAcceleratorEnabled': False, 'id': None, 'name': 'hana'+str(y)+'-s'+str(x)+'_DataDisk_0', 'storageAccountType': None, 'diskSizeGB': None}]
            disk_list2 = [{'name': 'hana'+str(y)+'-s'+str(x)+'_DataDisk_0', 'diskSizeGB': 200, 'sku': 'Premium_LRS', 'creationData': {'createOption': 'empty'}}]
            subnet_list = [{'name': 'default', 'properties': {'addressPrefix': '10.'+str(x)+'.0.0/24'}}]
            subnet_list2 = ['10.'+str(x)+'.0.0/24']
            data["parameters"]["networkInterfaceName"]["value"] = "hana"+str(y)+"-s"+str(x)+"-nic"
            data["parameters"]["publicIpAddressName"]["value"] = "hana"+ str(y)+"-s"+str(x)+"-ip"
            data["parameters"]["dataDisks"]["value"] = disk_list
            data["parameters"]["dataDiskResources"]["value"] = disk_list2
            data["parameters"]["virtualMachineName"]["value"] = "hana"+str(y)+"-s"+str(x)
            data["parameters"]["networkSecurityGroupName"]["value"] = "student"+str(x)+"-nsg"
            data["parameters"]["virtualMachineRG"]["value"] = "ho1088-rg-student"+str(x)
            data["parameters"]["diagnosticsStorageAccountName"]["value"] = "student"+str(x)+"sa"
            data["parameters"]["diagnosticsStorageAccountId"]["value"] = "Microsoft.Storage/storageAccounts/student"+str(x)+"sa"
            data["parameters"]["virtualNetworkName"]["value"] = "student"+str(x)+"-vnet"
            data["parameters"]["addressPrefixes"]["value"] = subnet_list2
            data["parameters"]["subnets"]["value"] = subnet_list
            with open('vm_all_parameter_files/hana'+str(y)+'-s'+str(x)+'.txt', 'w') as outfile:
                json.dump(data, outfile, indent=4)
